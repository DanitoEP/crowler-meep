package com.meep.crawler.service;

import com.meep.crawler.service.api.MeepResponse;

public class CrawlerTestUtils {

  public static MeepResponse createMeepResponse() {

    return MeepResponse.builder()
        .id("VR1URHNKKKW102778")
        .name("152_54ZF67")
        .x((long) -9.150108337402344)
        .y((long) 38.717430114746094)
        .licencePlate("152_54ZF67")
        .range(784)
        .batteryLevel(98)
        .seats(5)
        .model("null DS3")
        .resourceImageId("vehicle_gen_emov")
        .realTimeData(true)
        .resourceType("ELECTRIC_CAR")
        .companyZoneId(467)
        .build();
  }
}
