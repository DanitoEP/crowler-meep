package com.meep.crawler.service;

import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ScheduleCrawlerTest {

  @Mock
  private CrawlerService crawlerService;

  private ScheduleCrawler scheduleCrawler;

  @Before
  public void setUp() {

    scheduleCrawler = new ScheduleCrawler(crawlerService);
  }

  @Test
  public void GivenLowerLeftLatLonAndUpperRightLatLonAndCompanyZoneIds_WhenScheduleCrawler_ThenVerifyCallCraweler() {
    // Given
    final String lowerLeftLatLonExpected = "38.711046,-9.160096";
    final String upperRightLatLonExpected = "38.739429,-9.137115";
    final String companyZoneIdsExpected = "545,467,473";

    // When
    scheduleCrawler.scheduleCrawler();

    // Then
    verify(crawlerService).crawler(lowerLeftLatLonExpected, upperRightLatLonExpected, companyZoneIdsExpected);
  }
}