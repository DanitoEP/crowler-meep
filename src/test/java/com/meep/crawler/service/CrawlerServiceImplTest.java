package com.meep.crawler.service;

import static com.meep.crawler.persistence.RedisRepository.vehicles;
import static com.meep.crawler.service.CrawlerTestUtils.createMeepResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.meep.crawler.service.api.MeepApi;
import com.meep.crawler.service.api.MeepResponse;
import com.meep.crawler.service.domain.Vehicle;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CrawlerServiceImplTest {

  @Mock
  private MeepApi meepApi;

  private CrawlerService crawlerService;

  @Before
  public void setUp() {

    crawlerService = new CrawlerServiceImpl(meepApi);
  }

  @Test
  public void GivenALowerLeftLatLonAndUpperRightLatLonAndCompanyZoneIds_WhenCrawler_ThenUpdateList() {
    // Given
    final String lowerLeftLatLon = "38.711046,-9.160096";
    final String upperRightLatLon = "38.739429,-9.137115";
    final String companyZoneIds = "545,467,473";
    final MeepResponse meepResponse = createMeepResponse();
    when(meepApi.getLisboaResources(lowerLeftLatLon, upperRightLatLon, companyZoneIds)).thenReturn(
        List.of(meepResponse));
    final Vehicle vehiclePersisted = new Vehicle(meepResponse.toBuilder()
        .id("oldId")
        .build()).toBuilder()
        .available(true)
        .build();
    vehicles = List.of(vehiclePersisted);

    // When
    crawlerService.crawler(lowerLeftLatLon, upperRightLatLon, companyZoneIds);

    // Then
    assertThat(vehicles).contains(vehiclePersisted.toBuilder()
        .available(false)
        .build());

    assertThat(vehicles).contains(new Vehicle(meepResponse).toBuilder()
        .available(true)
        .build());
  }
}