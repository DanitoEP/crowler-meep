package com.meep.crawler.service.api;

import static com.meep.crawler.service.CrawlerTestUtils.createMeepResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MeepApiTest {

  @Mock
  private MeepApi api;

  @Test
  public void GivenARequestParams_WhenGetLisboaResources_ThenReturnApiResponse() {

    // Given
    final MeepResponse meepResponse = createMeepResponse();
    when(api.getLisboaResources(anyString(), anyString(), anyString())).thenReturn(List.of(meepResponse));
    final String lowerLeftLatLon = "38.711046,-9.160096";
    final String upperRightLatLon = "38.739429,-9.137115";
    final String companyZoneIds = "545,467,473";

    // When
    List<MeepResponse> lisboaResources = api.getLisboaResources(lowerLeftLatLon, upperRightLatLon, companyZoneIds);

    //Then
    assertThat(lisboaResources).isNotNull();
    assertThat(lisboaResources.size()).isGreaterThan(0);
    assertThat(lisboaResources).contains(meepResponse);

  }

}