package com.meep.crawler.service.api;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class MeepApiIT {

  private static final String MEEP_API_URL = "https://apidev.meep.me/tripplan/api/v1";

  private MeepApi api;

  @Before
  public void setUp() {

    api = MeepApi.instance(MEEP_API_URL);
  }

  @Test
  public void GivenARequestParams_WhenGetLisboaResources_ThenReturnApiResponse() {
    // Given
    final String lowerLeftLatLon = "38.711046,-9.160096";
    final String upperRightLatLon = "38.739429,-9.137115";
    final String companyZoneIds = "545,467,473";

    // When
    final List<MeepResponse> lisboaResources = api.getLisboaResources(lowerLeftLatLon, upperRightLatLon,
        companyZoneIds);

    // Then
    assertThat(lisboaResources).isNotNull();
    assertThat(lisboaResources.size()).isGreaterThan(0);
  }

  @Test
  public void GivenAEmptyLowertLeftParams_WhenGetLisboaResources_ThenReturnApiResponse() {
    // Given
    final String lowerLeftLatLon = "";
    final String upperRightLatLon = "38.739429,-9.137115";
    final String companyZoneIds = "545,467,473";

    // When
    final List<MeepResponse> lisboaResources = api.getLisboaResources(lowerLeftLatLon, upperRightLatLon,
        companyZoneIds);

    // Then
    assertThat(lisboaResources).isNotNull();
    assertThat(lisboaResources.size()).isGreaterThan(0);
  }

  @Test
  public void GivenAEmptyCompanyZoneIdsParams_WhenGetLisboaResources_ThenReturnApiResponse() {
    // Given
    final String lowerLeftLatLon = "38.711046,-9.160096";
    final String upperRightLatLon = "38.739429,-9.137115";
    final String companyZoneIds = "";

    // When
    final List<MeepResponse> lisboaResources = api.getLisboaResources(lowerLeftLatLon, upperRightLatLon,
        companyZoneIds);

    // Then
    assertThat(lisboaResources).isNotNull();
    assertThat(lisboaResources.size()).isGreaterThan(0);
  }
}