package com.meep.crawler.persistence;

import com.meep.crawler.service.domain.Vehicle;
import java.util.ArrayList;
import java.util.List;

/**
 * This class does not connect to a Redis, it simply has a static object where all the data is stored. In a final
 * implementation a Redis would have to be connected
 **/

public class RedisRepository {

  //This object can be converted for later use with more ease in your own api rest
  //I propose to use mapstruct to convert objects
  public static List<Vehicle> vehicles = new ArrayList<>();
}
