package com.meep.crawler.service;

public interface CrawlerService {

  void crawler(String lowerLeftLatLon, String upperRightLatLon, String companyZoneIds);
}
