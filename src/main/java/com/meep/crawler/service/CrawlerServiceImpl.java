package com.meep.crawler.service;

import com.meep.crawler.persistence.RedisRepository;
import com.meep.crawler.service.api.MeepApi;
import com.meep.crawler.service.api.MeepResponse;
import com.meep.crawler.service.domain.Vehicle;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CrawlerServiceImpl implements CrawlerService {

  private final MeepApi meepApi;

  @Override
  public void crawler(final String lowerLeftLatLon, final String upperRightLatLon, final String companyZoneIds) {

    final List<MeepResponse> lisboaResources = meepApi.getLisboaResources(lowerLeftLatLon,
        upperRightLatLon, companyZoneIds);

    final List<Vehicle> unavailable = RedisRepository.vehicles.stream()
        .filter(vehicle -> lisboaResources.stream()
            .noneMatch(meepResponse -> meepResponse.getId()
                .equals(vehicle.getId())))
        .map(vehicle -> vehicle.toBuilder()
            .available(false)
            .build())
        .collect(Collectors.toList());

    final List<Vehicle> available = lisboaResources.stream()
        //Maybe at this point you should make a converter, with the necessary data that you want to persist
        .map(meepResponse -> new Vehicle(meepResponse).toBuilder()
            .available(true)
            .build())
        .collect(Collectors.toList());

    RedisRepository.vehicles = Stream.concat(unavailable.stream(), available.stream())
        .collect(Collectors.toList());


  }

}
