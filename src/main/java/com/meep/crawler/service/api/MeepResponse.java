package com.meep.crawler.service.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
public class MeepResponse {

  private String id;

  private String name;

  private long x;

  private long y;

  private String licencePlate;

  private int range;

  private int batteryLevel;

  private int seats;

  private String model;

  private String resourceImageId;

  private boolean realTimeData;

  //This must be an enum
  private String resourceType;

  private int companyZoneId;
}
