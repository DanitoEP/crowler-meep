package com.meep.crawler.service.api;


import feign.Feign;
import feign.Headers;
import feign.Logger;
import feign.Param;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;
import java.util.List;

@Headers(value = {"Content-Type: application/json", "Accept: application/json"})
public interface MeepApi {

  static MeepApi instance(final String urlBase) {

    return Feign.builder()
        .logger(new Slf4jLogger())
        .logLevel(Logger.Level.FULL)
        .decoder(new JacksonDecoder())
        .encoder(new JacksonEncoder())
        .target(MeepApi.class, urlBase);
  }

  @RequestLine("GET /routers/lisboa/resources?lowerLeftLatLon={lowerLeftLatLon}&upperRightLatLon={upperRightLatLon}&companyZoneIds={companyZoneIds}")
  List<MeepResponse> getLisboaResources(@Param(value = "lowerLeftLatLon") String lowerLeftLatLon,
      @Param(value = "upperRightLatLon") String upperRightLatLon,
      @Param(value = "companyZoneIds") String companyZoneIds);
}
