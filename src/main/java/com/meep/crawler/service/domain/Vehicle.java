package com.meep.crawler.service.domain;

import com.meep.crawler.service.api.MeepResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
public class Vehicle extends MeepResponse {

  public Vehicle(MeepResponse meepResponse) {

    setId(meepResponse.getId());
    setName(meepResponse.getName());
    setX(meepResponse.getX());
    setY(meepResponse.getY());
    setLicencePlate(meepResponse.getLicencePlate());
    setRange(meepResponse.getRange());
    setBatteryLevel(meepResponse.getBatteryLevel());
    setSeats(meepResponse.getSeats());
    setModel(meepResponse.getModel());
    setResourceImageId(meepResponse.getResourceImageId());
    setRealTimeData(meepResponse.isRealTimeData());
    setResourceType(meepResponse.getResourceType());
    setCompanyZoneId(meepResponse.getCompanyZoneId());
  }

  private boolean available;
}
