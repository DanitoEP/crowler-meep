package com.meep.crawler.service;

import static com.meep.crawler.persistence.RedisRepository.vehicles;

import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ScheduleCrawler {

  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");

  private final CrawlerService crawlerService;

  @Scheduled(fixedRate = 30000)
  public void scheduleCrawler() {

    final String lowerLeftLatLon = "38.711046,-9.160096";
    final String upperRightLatLon = "38.739429,-9.137115";
    final String companyZoneIds = "545,467,473";

    log.info("Call crawler in time {}", DATE_FORMAT.format(new Date()));
    crawlerService.crawler(lowerLeftLatLon, upperRightLatLon, companyZoneIds);
    log.info("Finish call crawler in time {}, vehicles {}", DATE_FORMAT.format(new Date()), vehicles);
  }
}
