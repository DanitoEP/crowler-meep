package com.meep.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {"com.meep.crawler"})
@EnableScheduling
public class CrawlerApplication {

  @Bean
  public static PropertySourcesPlaceholderConfigurer configurer() {

    return new PropertySourcesPlaceholderConfigurer();
  }

  public static void main(final String[] args) {

    SpringApplication.run(CrawlerApplication.class, args);
  }

}
