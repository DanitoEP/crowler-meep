package com.meep.crawler.config;

import com.meep.crawler.service.api.MeepApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MeepApiConfiguration {

  @Value("${meep.url.base}")
  private String meepUrlBase;

  @Bean
  public MeepApi documentApi() {

    return MeepApi.instance(meepUrlBase);
  }

}
